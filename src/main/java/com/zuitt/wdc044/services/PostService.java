package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(String stringToken, Post post);

    Iterable<Post> getPosts();

    Iterable<Post> getUserPosts(String strToken);

    ResponseEntity<Object> updatePost(Long id, String strToken, Post post);

    ResponseEntity<Object> deletePost(Long id, String strToken);
}

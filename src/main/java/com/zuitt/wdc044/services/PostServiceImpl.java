package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Iterator;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    /**
     * method for creating post object
     * @param stringToken The token
     * @param post The post to create
     */
    @Override
    public void createPost(String stringToken, @NotNull Post post) {
        // retrieves the user
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        postRepository.save(newPost);
    }

    /**
     * Retrieves all the posts
     * @return Posts
     */
    @Override
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    @Override
    public Iterable<Post> getUserPosts(String strToken) {
        String username = jwtToken.getUsernameFromToken(strToken);
        User user = userRepository.findByUsername(username);

        Iterable<Post> posts = postRepository.findAllByUser(user);
        for (Post post : posts) {
            post.getUser().setPassword("encrypted");
        }

        return posts;
    }

    @Override
    public ResponseEntity<Object> updatePost(Long id, String strToken, Post post) {
        /*
          Logic:
            - When updating post, find the post by id.
            - Check if the user who is trying to update the post is the one who owns/created the post.
            - If the user is the owner of the post, then update the post, else don't update it.
         */

        Post postForUpdating = postRepository.findById(id).get();

        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(strToken);

        if (authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                "You are not authorized to edit this post", HttpStatus.UNAUTHORIZED
            );
        }
    }

    @Override
    public ResponseEntity<Object> deletePost(Long id, String strToken) {
        Post postForDeleting = postRepository.findById(id).get();

        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(strToken);

        if (authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("You have deleted your post.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post", HttpStatus.UNAUTHORIZED);
        }
    }
}

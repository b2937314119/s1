package com.zuitt.wdc044.models;

import java.io.Serial;
import java.io.Serializable;
//This model will be used for creating the JWT using the request body contents
//In out AuthController
public class JwtRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 1730993934201187817L;

    private String username;

    private String password;

    public JwtRequest(){}

    public JwtRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
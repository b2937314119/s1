package com.zuitt.wdc044.models;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {

    // @Id - to indicate that this property is the primary key
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // generate or increments the value
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false) // defines the foreign key in our posts table
    private User user;

    public Post(){}

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
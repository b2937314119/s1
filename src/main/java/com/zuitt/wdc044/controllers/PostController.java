package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    /**
     * Creates a post
     * @param stringToken the token
     * @param post the post to be created
     * @return an object that tells if the post has been created or not.
     */
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    // ResponseEntity represents the whole HTTP response: status code, headers, and body
    public ResponseEntity<Object> createPost(
        @RequestHeader(value = "Authorization") String stringToken,
        @RequestBody Post post
    ) {
        postService.createPost(stringToken, post);

        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    /**
     * Retrieves all the posts
     * @return all posts
     */
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserAllPosts(@RequestHeader(value = "Authorization") String strToken) {
        return new ResponseEntity<>(postService.getUserPosts(strToken), HttpStatus.OK);
    }

    /**
     * Updates a post
     * @param postId the post id
     * @param strToken the token
     * @param post the new post
     * @return an object that tells if the post has been updated or not
     */
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(
        @PathVariable Long postId, //@PathVariable used for the data passed in the URL/endpoint
        @RequestHeader(value = "Authorization") String strToken,
        @RequestBody Post post
    ) {
        return postService.updatePost(postId, strToken, post);
    }

    /**
     * Delete a post
     * @param postId id of the post
     * @param strToken token
     * @return the post object
     */
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(
        @PathVariable Long postId,
        @RequestHeader(value = "Authorization") String strToken
    ) {
        return postService.deletePost(postId, strToken);
    }
}
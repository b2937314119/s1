package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.JwtRequest;
import com.zuitt.wdc044.models.JwtResponse;
import com.zuitt.wdc044.services.JwtUserDetailsService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

//@RestController - to tell spring boot this is a REST API controller.
//@CrossOrigin - to disable CORS
@RestController
@CrossOrigin
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtToken jwtToken;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    //Allows us to map an endpoint to a controller.
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody @NotNull JwtRequest authenticationRequest) throws Exception {
        //AuthenticationRequest is the request body instantiated as a JwtRequest Class.
        //@Request Body annotation allows us to get the request body and bind it to our parameter.

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        //load the username with the jwtUserDetailsService to add the user in the spring security scheme.
        //Then, use the return userDetails objects to create the token.
        final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        //Generate the token by passing the returned userDetails object.
        final String token = jwtToken.generateToken(userDetails);

        //send a response to the client with 200 status code and the response body containing an object from the JWT response class that bears/contains our token.
        return ResponseEntity.ok(new JwtResponse(token));

    }

    private void authenticate(String username, String password) throws Exception {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
